#this tutorial about how i can use python as calculator (simple)

print 2 + 2 #simple math format
print 50 - 5*6 #the seconed term mean 5 multply by 6
print (50 - 5.0*6)/4 #parentheses used for grouping
print 8/5.0

# real number without fraction such as 1 , 6 ,2 have type  int
# real number with fractin such as 1.5 ,5.0 have type float
#floor divisin (//) will return int value .will scale to the un fraction number

print 11 //4 # = 2
print -11 // 4 # = -3
print 17 / 3 #this one should return int value without fraction
print 17 / 3.0 #this one will return fraction number this the accurate result
print 17 % 3 #return the reminder i dont know its porpuse
print 5 ** 20 #return 5 to the power 20

# you can even to use assigned value to the variable

width = 20.88
hight = 5 * 9.84
print width * hight
print round(width * hight , 3) #the second will round the number to .xxx
