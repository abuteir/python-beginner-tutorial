#this one is likeyou script with argv
def print_two(*args): # "*args" simmilar of argv but for functions.
    arg1, arg2 = args
    print "arg1: %r, arg2: %r" %(arg1, arg2)

# ok, that *args i actually pointless, we can just do this

def print_two_again(arg1,arg2):
    print "arg1: %r, arg2: %r" %(arg1, arg2)

def  print_one(arg1):
    print "arg1: %r"%arg1

#no arguments

def print_none():
    print "I got nothing."

print_two("ahmed", "teir")
print_two_again("ahmed", "teir")
print_one("First")
print_none()
