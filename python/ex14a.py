def my_script(script="ex14a.py", user_name="ahmed",prompt = "> "):
    print "Hi %s, I'm the %s script."%  (user_name, script)
    print "I'd like to ask you a few question."
    print "Do you like me %s" % user_name ; likes = raw_input(prompt)

    print "Where do you live %s" % user_name ; lives = raw_input(prompt)

    print "What kind of computer do you have?" ; computer = raw_input(prompt)

    print """
    Dear %r, Alright, so you said %r about liking me.
    You live in %r. Not sure where that is .
    And you have %r computer. Nice.
    """ % (user_name, likes, lives, computer)

my_script()


# if you asign value to the variable in def function no need to assign agian when you recall the functions
