#me aware of spaces your program will not run if it is not will arrange
#list.remove(x) will remove item from list it's return error of there is no such item
#list.clear() or del list[start:end] remove the item from list or the list itself
print "Hello World"
print "Hello Again"
print "I like typing this."
print "This is fun."
print 'Yay ! printing.'
print "I'd much rather you 'not'."
print 'I "said" do not touch this.'
print "Hi I'm Ahmed."
mylist=["Hello World","Hello Again", "I like typing this.", "This is fun.", "Yay ! printing.", "I'd much rather you 'not'.", 'I "said" do not touch this.',"Hi I'm Ahmed."]
print mylist[0:2]# characters from position 0 (included) to 2 (excluded)
print mylist[3] #will print item #3
print mylist.pop(0) #list.pop(x) use for to remove item from list and return it
print mylist
del mylist[:]#list.clear() or del list[start:end] remove the item from list or the list itself
print mylist
